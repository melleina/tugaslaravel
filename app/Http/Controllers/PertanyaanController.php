<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('tugascrud.create');
    }

    public function store(Request $request){
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan');
    }

    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('tugascrud.index', compact('pertanyaan'));
    }

    public function show($id){
        $per1 = DB::table('pertanyaan')->where('id', $id)->first();
        return view('tugascrud.show', compact('per1'));
    }

    public function edit($id){
        $per2 = DB::table('pertanyaan')->where('id', $id)->first();

        return view('tugascrud.edit', compact('per2'));
    }

    public function update($id, Request $request){
        $query = DB::table('pertanyaan')->where('id', $id)
                -> update([
                    'judul' => $request['judul'],
                    'isi' => $request['isi']
                ]);

                return redirect('/pertanyaan')->with('success', 'Berhasil Update Pertanyaan');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Berhasil Menghapus Pertanyaan');
    }
}
