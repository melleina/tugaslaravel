<!DOCTYPE html>
<html>
	<head>
		<title> Form User </title>
	</head>

	<body>
		<h1> Buat Account Baru </h1>
		<h3> Sign Up Form </h3>

		<form action="/welcome" method="POST">
            @csrf
			<label for="first_name"> First Name : </label>
			<br><br>
			<input type="text" name="first" id="first_name">
			<br><br>
			<label for="last_name"> Last Name : </label>
			<br><br>
			<input type="text" name="last" id="last_name">
			<br><br>

			<label> Gender: </label> <br><br>
				<input type="radio" name="gender" value="0"> Male <br>
				<input type="radio" name="gender" value="1"> Female <br>
				<input type="radio" name="gender" value="2"> Other <br>
				<br>

			<label> Nationality: </label> <br><br>
				<select>
					<option value="ind">Indonesian</option>
					<option value="sin">Singaporean</option>
					<option value="my">Malaysian</option>
					<option value="aus">Australian</option>
				</select>
				<br><br>

			<label> Language Spoken: </label> <br><br>
				<input type="checkbox" name="lang" value="in"> Bahasa Indonesia <br>
				<input type="checkbox" name="lang" value="en"> English <br>
				<input type="checkbox" name="lang" value="ot"> Other <br>
				<br>

			<label for="bio_user"> Bio: </label> <br><br>
				<textarea cols="30" rows="10" id="bio_user"></textarea> <br>

			<input type="submit" value="Sign Up">
		</form>
	</body>
</html>